section .text

%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1
;%define READ_SYSCALL 0
%define STDOUT 1
;%define STDIN 0


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину в rax
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], 0
        je .ret
        inc rax
        jmp .loop
    .ret: ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, rsp            ; в rdi должен быть адрес
    call print_string
    pop rdi
    ret

; Принимает два указателя (rdi, rsi) на нуль-терминированные строки, возвращает в rax 1, если они равны, 0 иначе
string_equals:
    xor rax, rax
    cld                     ; сброс DF, для директного cmpsb
    .loop:
        cmpsb               ; cmp byte[rdi], byte[rsi]; inc rdi; inc rsi
        jne .ret
        cmp byte[rdi-1], 0  ; два символа равны, но равны ли они NUL-терминатору?
        jnz .loop
    inc rax
    .ret: ret

; Принимает в rdi указатель на нуль-терминированную строку, в rsi указатель на буфер и в rdx длину буфера
; Копирует строку в буфер вместе с нуль-терминатором
; Возвращает в rax: длину строки, если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rdi                 ; для movsb, rdi - буфер куда копировать,
    pop rsi                 ;            rsi - исходная строка
    cmp rax, rdx            ; длина строки > размера буфера?
    jge .error
    mov rcx, rax            ; rcx - счетчик символов, которые нужно скопировать (для rep)
    inc rcx                 ; чтоб скопировать вместе с терминатором
    cld                     ; сброс DF, для директного movsb
    rep movsb               ; перемещаем rсx байтов из строки в буфер
    ret
    .error:
        xor rax, rax
        ret

; Принимает указатель на нуль-терминированную строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx: его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax                ; rax - результат (число)
    xor rsi, rsi                ; rsi - длина числа
    xor rcx, rcx                ; rcx (cl) - текущий символ
    .loop:
        mov cl, byte[rdi+rsi]
        cmp cl, '0'
        jb .end
        cmp cl, '9'
        ja .end
        sub cl, '0'
        imul rax, 10            ; умножить число на 10
        add rax, rcx            ; прибавить к нему прочитанную цифру
        inc rsi
        jmp .loop
    .end:
        mov rdx, rsi
        ret

; Принимает указатель на нуль-терминированную строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx: его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    test rdx, rdx
    jz .ret
    inc rdx
    .ret: ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
; Выводит беззнаковое 8-байтовое число в десятичном формате
print_uint:
    mov rax, rdi            ; rax - частное, rdx - остаток
    mov r10, 10             ; r10 - делитель на 10
    push rbx                ; rbx - callee-saved
    mov rbx, rsp            ; rbx - бэкап rsp, чтобы в конце вернуться на исходную позицию
    dec rsp                 ; освобождаем место под символ, и сразу получается NUL-терминатор
    .loop:
        xor rdx, rdx
        div r10
        add rdx, '0'        ; перевод цифры в ASCII
        dec rsp
        mov byte[rsp], dl
        test rax, rax
        jnz .loop
    mov rdi, rsp
    call print_string
    mov rsp, rbx
    pop rbx
    ret

; Читает один символ из stdin и возвращает его в rax.
; Возвращает 0, если достигнут конец потока или произошла ошибка чтения.
read_char:
    xor rax, rax            ; read
    xor rdi, rdi            ; stdin
    push 0                  ; освобождаем место под символ
    mov rsi, rsp            ; читать в память по адресу из rsp
    mov rdx, 1              ; количество байт
    syscall
    cmp rax, -1             ; произошла ошибка чтения?
    pop rax
    jne .ret
    xor rax, rax            ; возвращаем 0
    .ret: ret

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале (0x20[space], 0x9[tab] и 0xA[newline]).
; Останавливается и возвращает 0 в rax, если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi            ; r12 - адрес буфера
    mov r13, rsi            ; r13 - размер буфера
    dec r13                 ; в буфере нужно место под терминатора
    xor r14, r14            ; r14 - текущая длина слова
    .loop:
        call read_char
        test rax, rax       ; EOF?
        je .success
        cmp rax, ' '
        je .check_success
        cmp rax, `\t`
        je .check_success
        cmp rax, `\n`
        je .check_success
        cmp r14, r13
        jge .error
        mov [r12+r14], rax
        inc r14
        jmp .loop
    .check_success:
        test r14, r14       ; что-то уже прочитано?
        jz .loop
    .success:
        mov byte[r12+r14+1], 0
        jmp .end
    .error:
        xor r12, r12
    .end:
        mov rax, r12
        mov rdx, r14
    pop r14
    pop r13
    pop r12
    ret
